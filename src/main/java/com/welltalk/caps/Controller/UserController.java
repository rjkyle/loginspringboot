package com.welltalk.caps.Controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.welltalk.caps.Entity.UserEntity;
import com.welltalk.caps.Repository.UserRepository;
import com.welltalk.caps.Service.UserService;

import DTO.ForgotPasswordRequest;

@RestController
@RequestMapping("/user")

@CrossOrigin(origins = "http://localhost:19006")
public class UserController {

    @Autowired
    private UserService userService; // Autowire the UserRepository

    @PostMapping("/signup")
    public ResponseEntity<String> signup(@RequestBody UserEntity user) {
        return userService.signup(user); // Call the non-static method on the instance
    }
    @GetMapping("/getByUserid")
    public ResponseEntity findByUserid(
            @RequestParam(name = "userid", required = false, defaultValue = "0") Long userid,
            @RequestParam(name = "password", required = false, defaultValue = "0") String password	
    ) {
    	    
    	    
        UserEntity user = userService.findByUserid(userid);

        if (user != null && user.getPassword().equals(password)) {
            // Log-in successful
            return ResponseEntity.ok("Log-in successful");
        } else {
            // Log-in invalid
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Log-in invalid");
        }
    }
    @PostMapping("/forgotpassword")
    public ResponseEntity<String> forgotPassword(@RequestBody ForgotPasswordRequest request) {
        // Here, you would implement the logic to send a password reset email
        // using the provided email address (request.getEmail())
        // You may generate a reset token, send an email with a reset link, and store the token in a database.
        // Return an appropriate response based on whether the email was sent successfully.
        // You can use a service or library to send emails (e.g., JavaMailSender or an email API).

        // For simplicity, let's assume the email was sent successfully.
        return ResponseEntity.ok("Password reset email sent successfully.");
    }
    
}